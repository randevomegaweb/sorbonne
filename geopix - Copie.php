<?php

/** Sets up the WordPress Environment. */
require(dirname(__FILE__) . '/wp-load.php');

add_action('wp_head', 'wp_no_robots');

require(dirname(__FILE__) . '/wp-blog-header.php');

nocache_headers();

global $randev_state_ow;
$randev_state_ow = true;
get_header();

?>

    <script>
        jQuery(function () {
            jQuery(".content-outer.description_top").css('display', 'none');
            jQuery(".home_bloc_img_ow_content").css('display', 'none');
        });
    </script>

    <div class="row geopix_main_content"
         style="width:90%;">

        <?php
        global $wpdb;
        global $wp_query;
        $query = "SELECT * FROM srb_wp_posts ";
        $gId = $_GET['galleryid']; //var_dump($gId);
        $itemId = $_GET['itemid']; //var_dump($gId);

        $queryItemById = " SELECT * FROM item  WHERE id= $itemId LIMIT 1";
        $queryItemAll = " SELECT * FROM item  LIMIT 100 ";
        $queryGalleryById = " SELECT * FROM srb_wp_ngg_gallery  WHERE gid= $gId LIMIT 1";
        $queryPictures = " SELECT * FROM srb_wp_ngg_pictures  WHERE galleryid= $gId LIMIT 20";

        $query_select_image = "
  SELECT
  photo.url as filename,
  liencat.idGallery,
  srb_wp_ngg_gallery.path,
  lienphoto.id_photo,
  lienphoto.id_item as idItem
  FROM
  lienphoto
  INNER JOIN photo ON photo.id = lienphoto.id_photo
  INNER JOIN liencat ON lienphoto.id_item = liencat.idItem
  INNER JOIN srb_wp_ngg_gallery ON liencat.idGallery = srb_wp_ngg_gallery.gid
  WHERE
  liencat.idItem = $itemId AND
  photo.url IS NOT NULL
  GROUP BY
  lienphoto.id_photo
  LIMIT 20
  ";

        $allmiles = $wpdb->get_results($wpdb->prepare($query));
        $allPictures = $wpdb->get_results($wpdb->prepare($queryPictures));
        $galleryObj = $wpdb->get_results($wpdb->prepare($queryGalleryById));
        $itemObj = $wpdb->get_results($wpdb->prepare($queryItemById));
        $itemAllObj = $wpdb->get_results($wpdb->prepare($queryItemAll));
        $allPictures_Item = $wpdb->get_results($wpdb->prepare($query_select_image));
        //var_dump($itemObj);


        //$base_url_randev = "http://localhost/sorbonne/";
        $base_url_randev = "http://sorbonne.projets-omega-web.net/";
        //$path = $base_url_randev.$galleryObj[0]->path;

        ?>

        <!-- CSS -->
        <link href="<?= $base_url_randev; ?>utils/custom.css"
              rel="stylesheet">

        <!-- Font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
              rel="stylesheet">


        <div id="geopix_header_content"
             class="ml-5"
             style="z-index: 9 !important;">

            <div class="row"
                 style="margin: 20px;">
                <div class="col-lg-12 ">
                    <!--<div class="row top">-->
                    <div class="col-lg-6 p-0">
                        <h4 class="pageTitle"
                            class="display-4"><?= $itemObj[0]->titre ?></h4>
                        <div class="paragraphe"
                             id="para">
                            <?php
                            echo $itemObj[0]->descr;
                            ?>
                        </div>
                        <div class=" btn btn-link w-100"
                             id="button">
                            Lire
                            la
                            suite
                        </div>
                    </div>
                    <div class="col-lg-6">
                    </div>
                    <!--</div>-->
                </div>
            </div>

            <div class="row"
                 style="margin: 20px; ">


                <div class="col-md-6">
                    <?php
                    $queryPictures_first = "
        SELECT
        photo.url,
        liencat.idGallery,
        srb_wp_ngg_gallery.path,
        lienphoto.id_photo,
        lienphoto.id_item
        FROM
        lienphoto
        INNER JOIN photo ON photo.id = lienphoto.id_photo
        INNER JOIN liencat ON lienphoto.id_item = liencat.idItem
        INNER JOIN srb_wp_ngg_gallery ON liencat.idGallery = srb_wp_ngg_gallery.gid
        WHERE
        liencat.idItem = $itemId AND
        photo.url IS NOT NULL
        GROUP BY
        lienphoto.id_photo
        LIMIT 1
        ";
                    $allPictures_first = $wpdb->get_results($wpdb->prepare($queryPictures_first));
                    if (isset($allPictures_first[0]->url)) {
                        ?>
                        <img src="<?= $allPictures_first[0]->path . $allPictures_first[0]->url ?>"
                             class="img-fluid"
                             id="image"
                             alt="Responsive image">
                    <?php } ?>
                </div>
                <!--<div class="col">

                </div>-->

                <div class="col-md-6">
                    <div class="row">
                        <?php /*var_dump($allPictures_Item);*/
                        foreach ($allPictures_Item as $key => $picId) : ?>
                            <?php if ($key != 0) : ?>
                                <div class="col-md-4">
                                    <img src="<?= $picId->path . $picId->filename ?>"
                                         class="responsiveImage pb-3 image"
                                         alt="Responsive image">
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>

                    </div>
                </div>
            </div>

            <div class="row bottom"
                 style="margin: 20px; ">
                <div class="col-md-2">
                    <h5 class="title">
                        Photographe
                        :</h5>
                    <div class="diveder"></div>
                    <br>
                    <?= $itemObj[0]->photographe ?>

                </div>
                <div class="col-md-10">
                    <h5 class="title">
                        Source
                        :</h5>
                    <div class="diveder"></div>
                    <br>
                    <?= $itemObj[0]->source ?>
                </div>
            </div>


            <div class="col-lg-12 div_search_geopix"
                 id="div_search_geopix">
                <form action="<?= $_SERVER['REQUEST_URI'] ?>#div_search_geopix"
                      method="post"
                      id="form_search_geopix">
                    <input name="input_search_geopix"
                           class="input_search_geopix"
                           id="input_search_geopix"
                           placeholder="Ressources, matériaux et environnement - Ressources"/>
                    <button id="button_search_geopix"
                            type="submit">
                        <img src="wp-content/themes/responsive/images/search_ico.png"/>
                    </button>
                </form>
            </div>

        </div>


        <div id="exampleSlider"
             style="width:100%; z-index: 8 !important;">
            <div class="MS-content">

                <?php
                $queryAllItemByGallery = " 
      SELECT
      liencat.idItem,
      item.titre,
      liencat.idGallery
      FROM
      item
      INNER JOIN liencat ON item.id = liencat.idItem
      WHERE
      liencat.idGallery = " . $allPictures_first[0]->idGallery . "
      ORDER BY
      item.titre ASC
      LIMIT 10
      ";
                $ObjAllItemByGallery = $wpdb->get_results($wpdb->prepare($queryAllItemByGallery));
                ?>

                <?php foreach ($ObjAllItemByGallery as $itemObj) : ?>
                    <?php
                    $queryPictures_first_itemObj = "
        SELECT
        photo.url,
        photo.id,
        liencat.idGallery,
        srb_wp_ngg_gallery.path
        FROM
        lienphoto
        INNER JOIN photo ON photo.id = lienphoto.id_photo
        INNER JOIN liencat ON lienphoto.id_item = liencat.idItem
        INNER JOIN srb_wp_ngg_gallery ON liencat.idGallery = srb_wp_ngg_gallery.gid
        WHERE
        liencat.idItem = " . $itemObj->idItem . "
        ";
                    $allPictures_first_itemObj = $wpdb->get_results($wpdb->prepare($queryPictures_first_itemObj));
                    ?>
                    <?php if (isset($allPictures_first_itemObj[0]->url)) { ?>
                        <div class="item">
                            <a href="<?= $base_url_randev ?>geopix.php?itemid=<?= $itemObj->idItem ?>">
                                <p><?= $itemObj->titre ?></p>
                                <img src="<?= $allPictures_first_itemObj[0]->path . $allPictures_first_itemObj[0]->url ?>"
                                     class="img-fluid"
                                     alt="Responsive image">
                            </a>
                        </div>
                    <?php } ?>
                <?php endforeach ?>

            </div>
            <div class="MS-controls">
                <button class="MS-left">
                    <i class="fa fa-chevron-left"
                       aria-hidden="true"></i>
                </button>
                <button class="MS-right">
                    <i class="fa fa-chevron-right"
                       aria-hidden="true"></i>
                </button>
            </div>
        </div>


        <!-- Include jQuery -->
        <script src="<?= $base_url_randev; ?>utils/jquery-2.2.4.min.js"></script>

        <!-- Include Multislider -->
        <script src="<?= $base_url_randev; ?>utils/multislider.js"></script>

        <!-- Initialize element with Multislider -->
        <script>
            $('#exampleSlider').multislider({
                interval: 3000,
                slideAll: true,
                duration: 1500
            });

            $(document).ready(function () {
                //$('.layers__triggers').hide();
                $(".image").on('click', function (e) {
                    var attribute = $(this).attr('src');
                    $('#image').attr('src', attribute)
                })
                $('#button').on('click', function (e) {
                    // e.preventDefalut();
                    $("div").removeClass('paragraphe');
                    $(this).hide();
                })
            })
        </script>

        <style>
            @font-face {
                font-family: "OpenSansOW";
                src: url("/wp-content/themes/responsive/core/css/fonts/OpenSans-Regular.ttf");
            }

            .results_search_geopix {
                width: 90%;
                z-index: 7 !important;
            }

            .image {
                cursor: pointer;
            }

            .MS-left,
            .MS-right {
                position: absolute;
                top: 85% !important;
            }

            .MS-right {
                margin-right: 121px !important;
            }

            .paragraphe {
                width: 100%;
                height: 150px;
                overflow: hidden;
                padding: 5px;
                margin: 0;
                text-align: justify;
            }

            #para {
                text-align: justify;
            }

            .top {
                margin: 6px 30px;
            }

            .top .col-lg-6 {
                margin-left: 50px;
                width: 100%;
            }

            .pageTitle {
                padding: 10px;
                background: #1e276a;
                /* width: auto;*/
                width: 50%;
                height: auto;
                color: white;
                text-align: center;
                font-family: "OpenSansOw";
                font-size: 20px;
                padding-top: 15px;
                padding-bottom: 15px;
            }

            .right {
                margin-top: 20px;
            }

            .diveder {
                width: 45px;
                height: 5px;
                background: #1e276a;
                border: 2px solid #1e276a;
            }

            .bottom {
                margin: 20px 30px;
            }

            .btn {
                font-size: 17px !important;
            }

            div {
                font-family: "OpenSansOW";
                font-size: 16px;
            }

            .pl-5 {
                margin-top: 229px !important;
            }

            .title {
                color: #1e276a;
                font-size: 18px;
            }

            .col-lg-10 {
                width: 100% !important;
            }

            .col {
                width: 100% !important;
                padding: 0 !important;
            }

            .btn.btn-link {
                text-align: right;
            }

            #exampleSlider .MS-content .item p {
                font-size: 14px !important;
            }

            #exampleSlider .MS-content .item {
                display: inline-block;
                height: 100%;
                overflow: hidden;
                position: relative;
                vertical-align: top;
                border: 20px solid #e8e8e8 !important;
                border-right: none;
                width: 20% !important;
                background: #e8e8e8;
            }

            #exampleSlider .MS-left, .MS-right {
                top: 83% !important
            }

            #exampleSlider .MS-controls .MS-right {
                right: 75px;
            }

            #exampleSlider .MS-content {
                margin: 0px 5% 15px;
            }

            .div_search_geopix #button_search_geopix img {
                height: 40px;
            }

            #button_search_geopix {
                padding: 0;
                margin: 0;
                border: none;
                height: 40px;
                float: left;
            }

            .div_search_geopix {
                margin-left: 2.95%;
                padding-left: 0 !important;
                height: 40px;
            }

            #input_search_geopix {
                height: 40px;
                background-color: black;
                color: white;
                width: 385px;
                float: left;
                border: none;
                padding-left: 15px;
                font-weight: bold;
                font-family: "OpenSansOW";
            }

            .home #header_section .header-logo-left {
                background-color: #1e276a !important;
                background-image: none !important;
            }

            .menu-main-menu-container #menu-main-menu.menu {
                top: 35px;
            }
        </style>

    </div>

<?php
global $wpdb;
global $wp_query;
$input_search_geopix = $_POST['input_search_geopix'];
//var_dump($input_search_geopix);
if (isset($input_search_geopix) && $input_search_geopix != "") { ?>
    <div class="results_search_geopix"
         id="results_search_geopix" style="width: 81%; margin-left: 50px;">
        <?php

        $geosearch_search_exploded = explode(" ", $input_search_geopix);
        $x = 0;
        foreach ($geosearch_search_exploded as $geosearch_search_each) {
            $x++;
            $geosearch_construct = " ";
            if ($x == 1) {
                $geosearch_construct .= " titre LIKE '%$geosearch_search_each%' ";
                $geosearch_construct .= " OR descr LIKE '%$geosearch_search_each%' ";
            } else {
                $geosearch_construct .= " OR titre LIKE '%$geosearch_search_each%' ";
                $geosearch_construct .= " OR descr LIKE '%$geosearch_search_each%' ";
            }
        }
        $geosearch_construct = " SELECT * FROM item WHERE $geosearch_construct LIMIT 20";
        $geosearch_all = $wpdb->get_results($wpdb->prepare($geosearch_construct));
        //var_dump($geosearch_all);
        ?>
        <?php foreach ($geosearch_all as $geosearch_itemObj) { ?>
            <?php
            $geosearch_queryPictures_first_itemObj = "
        SELECT
        photo.url,
        photo.id,
        liencat.idGallery,
        srb_wp_ngg_gallery.path
        FROM
        lienphoto
        INNER JOIN photo ON photo.id = lienphoto.id_photo
        INNER JOIN liencat ON lienphoto.id_item = liencat.idItem
        INNER JOIN srb_wp_ngg_gallery ON liencat.idGallery = srb_wp_ngg_gallery.gid
        WHERE
        liencat.idItem = " . $geosearch_itemObj->id . "
        LIMIT 1
        ";
            $geosearch_allPictures_first_itemObj = $wpdb->get_results($wpdb->prepare($geosearch_queryPictures_first_itemObj));
            ?>
            <?php if (isset($geosearch_allPictures_first_itemObj[0]->url)) { ?>
                <div class="geosearch_item row w-100 m-2"
                     style="background-color:#cccccc; z-index: 10 !important;">
                    <div class="col-lg-1 p-3">
                        <a href="<?= $base_url_randev ?>geopix.php?itemid=<?= $geosearch_itemObj->idItem ?>">
                            <img src="<?= $geosearch_allPictures_first_itemObj[0]->path . $geosearch_allPictures_first_itemObj[0]->url ?>"
                                 class="img-fluid"
                                 alt="Responsive image">
                        </a>
                    </div>
                    <div class="col-lg-11">
                        <div class="w-100"><?= $geosearch_itemObj->titre ?></div>
                        <div class="w-100"><?= strip_tags(substr(htmlspecialchars_decode($geosearch_itemObj->descr), 1, 900)) . " (...)" ?></div>
                        <div class="w-100 text-right font-weight-bold"><a href="<?= $base_url_randev ?>geopix.php?itemid=<?= $geosearch_itemObj->idItem ?>">Lire la suite</a></div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>

<?php
get_footer();
?>