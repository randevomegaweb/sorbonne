<?php
define('WP_AUTO_UPDATE_CORE', 'minor');// Ce paramètre est requis pour garantir le bon traitement des mises à jour WordPress dans WordPress Toolkit. Supprimez cette ligne si ce site Web WordPress n'est plus géré par WordPress Toolkit.
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sorbonne1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3308' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-l2P(}4g%g4$fp3!j@{6Dk&|rEwlK[Vq+7ajGr+Rm$~FSc!d#A2_^zN)Do@;tLW^' );
define( 'SECURE_AUTH_KEY',  'SEs,bPd?J>;8fn3BsoMNJR/70Mjod3$N;XpoS.=pEGcL& r`8%@**I?* Ordcluq' );
define( 'LOGGED_IN_KEY',    '?k<5^C1=zc*FK9`=`Z/gDs HGdu2R.0>6/A71f?ba#~8g-:-p,S4If==kS>.wp*C' );
define( 'NONCE_KEY',        '335HT.D.xfB$b$_&:n#KIVc}PgOI/S1`=ChH}|zngsB;<{|#Z:{Nn+M`6 )+&r9G' );
define( 'AUTH_SALT',        '];83,Af>(Qu(K4X5@v+D1qQlk^Scv~]:gMv[dUWz%gj6E</0?N }s%}s/6e>(Q.S' );
define( 'SECURE_AUTH_SALT', '3hCp.:c yP@ |&#{SOiAOv,|V@6Oy.2E?|=!{E<mn{ZacY]/<fzU~k&ZOC-wP:oJ' );
define( 'LOGGED_IN_SALT',   '(cY*33)+l~;picgWl:UyV1Z2+~h3N5!Lh8m9%&%P%u*E4aBVIb*fHJf:We>7*XV]' );
define( 'NONCE_SALT',       'mw7-t}8+N,9O!1Ffs}xbR<bhJU6f7 w$]Wg-@uvJ+h1j?BrmE}5LsK3$&d8_?BCM' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'srb_wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
