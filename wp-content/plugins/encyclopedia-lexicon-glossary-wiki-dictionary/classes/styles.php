<?php Namespace WordPress\Plugin\Encyclopedia;

abstract class Styles {

  public static function init(){
    add_Action('admin_enqueue_scripts', [static::class, 'registerDashboardStyles']);
  }

  public static function registerDashboardStyles(){
    WP_Enqueue_Style('encyclopedia-dashboard-extension', Core::$base_url . '/assets/css/dashboard.css');
  }

}

Styles::init();
