<?php
$controller = C_Display_Type_Controller::get_instance();
$storage    = C_Gallery_Storage::get_instance();

$effect_code = $controller->get_effect_code($gallery->displayed_gallery);
$settings    = $gallery->displayed_gallery->get_entity()->display_settings;

echo $settings['widget_setting_before_widget']
     . $settings['widget_setting_before_title']
     . $settings['widget_setting_title']
     . $settings['widget_setting_after_title'];
?>
<?php // keep the following a/img on the same line ?>
<?php //var_dump($image);
    $inputow = array(2374,2376,2378,2383,2387,2385,2389,2391,2393,2404,2406,2412,2414,2423,2425,2431,2435,2441,2447,2449,2455,2457,2477,2480,2486,2488,2492,2494,2500,2502,2506,2514);
    
    //var_dump($rand_keysow);
    ?>
<div class="ngg-widget entry-content">
    <?php foreach ($images as $image) { ?>
        <?php $rand_keysow = array_rand($inputow, 1);?>
        <a href="/?page_id=<?php echo $inputow[$rand_keysow]; ?>"
           title="<?php echo esc_attr($image->alttext)?>"
           data-image-id='<?php echo esc_attr($image->pid); ?>'
           <?php //echo $effect_code ?>
           ><img title="<?php echo esc_attr($image->alttext)?>"
                 alt="<?php echo esc_attr($image->alttext)?>"
                 src="<?php echo esc_attr($storage->get_image_url($image, $settings['image_type'], TRUE)); ?>"
                 width="<?php echo esc_attr($settings['image_width']); ?>"
                 height="<?php echo esc_attr($settings['image_height']); ?>"
            /></a>
    <?php } ?>
</div>

<?php echo $settings['widget_setting_after_widget']; ?>
