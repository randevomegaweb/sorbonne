<?php
/**
 * Footer Template
 *
 * @file           footer.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Globalize Theme options
 */
global $responsive_options;
$responsive_options = responsive_get_options();
global $responsive_blog_layout_columns;
?>
<footer id="footer" class="clearfix" role="contentinfo" <?php responsive_schema_markup( 'footer' ); ?>>
	<?php responsive_footer_top(); ?>
	<?php get_sidebar( 'footer' ); ?>
	<div class="footer-bar grid col-940">
	<div class="content-outer">
	<div class="footer-layouts social-icon">
		<div class="copyright_annee">
                    <?php
					esc_attr_e( '&copy; Géopix ', 'responsive' );
					echo esc_attr( gmdate( 'Y' ) . ' ' );
					//echo responsive_get_social_icons_new() ;// phpcs:ignore ?>
	<!-- end of col-380 fit -->
		</div>
	<?php get_sidebar( 'colophon' ); ?>
	<?php
		echo '<div class="footer-layouts copyright">';
		
		?>
		<div class="powered">
		<div class="copyright_adresse">
		<a href="">Contributeurs</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><a href="http://www.ufr-teb.upmc.fr/fr/index.html" target="_blank">UFR TEB 918 4</a></strong>, place Jussieu 75005 Paris 
		</div>
		<ul>
		<li>
			<a href="#">
				<img src="<?php echo get_template_directory_uri();?>/images/fb.png" />
			</a>
		</li>
		<li>
			<a href="#">
				<img src="<?php echo get_template_directory_uri();?>/images/tw.png" />
			</a>
		</li>
		</ul>
		</div>
		<?php
		echo '</div>';
	?>
	</div>
</div>
	</div>

	<?php responsive_footer_bottom(); ?>
</footer><!-- end #footer -->
<?php responsive_footer_after(); ?>

</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook. ?>
<?php
if ( get_theme_mod( 'responsive_scroll_to_top' ) ) {
	$scroll_top_devices = get_theme_mod( 'responsive_scroll_to_top_on_devices', 'both' );
	?>
	<div id="scroll" title="Scroll to Top" data-on-devices="<?php echo esc_attr( $scroll_top_devices ); ?>">Top<span></span></div>
	<?php
}
?>


	<?php
	// If full screen mobile menu style.
	if ( 'fullscreen' === get_theme_mod( 'mobile_menu_style' ) ) {
		get_template_part( 'partials/mobile/mobile-fullscreen' );
	}

	// If sidebar mobile menu style.
	if ( 'sidebar' === get_theme_mod( 'mobile_menu_style' ) ) {
		get_template_part( 'partials/mobile/mobile-sidebar' );
	}
	?>
<?php wp_footer(); ?>
 <div class="layers">
      <div class="layers__triggers">
        <button onclick = "showAccueil();"  class="layers__trigger layers__trigger--1 is-active" data-layer-trigger="layer-1">
            <span>GEOPIX</span>
        </button>
        <button onclick = "showEnvir();"  class="layers__trigger layers__trigger--2 " data-layer-trigger="layer-2">
            <span>Environnement</span>
        </button>
        <button onclick = "showMat();" id='mate' class="layers__trigger layers__trigger--3 " data-layer-trigger="layer-3">
            <span>Matériaux</span>
        </button>
        <button onclick = "showRess();" id='ress' class="layers__trigger layers__trigger--4 " data-layer-trigger="layer-4">
            <span>Ressources <span class="d-none d-md-inline">&amp; Ingénierie</span></span>
        </button>
       </div>
    </div>
	<div class="layers__item layers__item--2" data-layer="layer-2">
	<div class="container">
contenu
    </div>
</div>	
<link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri(); ?>/core/css/style_anim.css" />
<script src="<?php echo get_template_directory_uri(); ?>/core/js/modernizr.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/core/js/anim2.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/core/js/anim1.js"></script>
<script>
	
	function showEnvir(){
		show_description_top_animate();
		setTimeout(function(){ 
			document.getElementById('page0').style.display = "none";
			document.getElementById('page1').style.display = "block";
			document.getElementById('page2').style.display = "none";
			document.getElementById('page3').style.display = "none";
			jQuery("header div.content-outer.description_top").css("margin-left", "78px");
			changeCSS_description_top();
		 }, 750);
	}
	function showMat(){
		show_description_top_animate();
		setTimeout(function(){
			document.getElementById('page0').style.display = "none";
			document.getElementById('page1').style.display = "none";
			document.getElementById('page2').style.display = "block";
			document.getElementById('page3').style.display = "none";
			jQuery("header div.content-outer.description_top").css("margin-left", "118px");
			changeCSS_description_top();
		}, 750);
	}
	function showRess(){
		show_description_top_animate();
		setTimeout(function(){
			document.getElementById('page0').style.display = "none";
			document.getElementById('page1').style.display = "none";
			document.getElementById('page2').style.display = "none";
			document.getElementById('page3').style.display = "block";
			jQuery("header div.content-outer.description_top").css("margin-left", "158px");
			changeCSS_description_top();
		}, 750);
	}
	function showAccueil(){
		show_description_top_animate();
		setTimeout(function(){
			show_description_top_animate();
			document.getElementById('page0').style.display = "block";
			document.getElementById('page1').style.display = "none";
			document.getElementById('page2').style.display = "none";
			document.getElementById('page3').style.display = "none";
			returnCSS_description_top();
		}, 750);
	}
	function changeCSS_description_top(){
		jQuery("header div.content-outer.description_top").css("background-color", "white");
		jQuery("header div.content-outer.description_top").css("color", "black");
		jQuery("header div.content-outer.description_top").css("width", "90%");
		jQuery("header div.content-outer.description_top h1").css("color", "black");
		jQuery("#wrapper").css("display", "none");
		jQuery(".content-outer #secondary #home_widget_1").css("display", "none");
		jQuery(".geopix_main_content").css("display", "none");
		jQuery(".home_bloc_img_ow_content").css("display", "none");
		jQuery("#exampleSlider .MS-content .item").css("z-index", "10 !important");
		jQuery("#geopix_header_content .row .col-md-6 .row .col-md-4").css("z-index", "9 !important");
		//alert('ok');
	}
	function returnCSS_description_top(){
		jQuery("header div.content-outer.description_top").css("background-color", "transparent");
		jQuery("header div.content-outer.description_top").css("color", "white");
		jQuery("header div.content-outer.description_top").css("width", "83%");
		jQuery("header div.content-outer.description_top h1").css("color", "white");
		jQuery("#wrapper").css("display", "block");
		jQuery(".content-outer #secondary #home_widget_1").css("display", "block");
		jQuery(".geopix_main_content").css("display", "block");
		jQuery(".home_bloc_img_ow_content").css("display", "flex");
		jQuery(".home #wrapper").css("display", "none");
		jQuery("#exampleSlider .MS-content .item").css("z-index", "10 !important");
		jQuery("#geopix_header_content .row .col-md-6 .row .col-md-4").css("z-index", "9 !important");
		//alert('non ok');
	}
	function show_description_top_animate(){
		jQuery("header div.content-outer.description_top").fadeOut("slow");
		jQuery("#wrapper").css("display", "none");
		jQuery("header div.content-outer.description_top").fadeIn("slow");
	}
	
</script>
	
</body>
</html>
