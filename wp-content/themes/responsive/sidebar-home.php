<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Home Widgets Template
 *
 * @file           sidebar-home.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/sidebar-home.php
 * @link           http://codex.wordpress.org/Theme_Development#secondary_.28sidebar.php.29
 * @since          available since Release 1.0
 */
?>
<?php
	$responsive_options = responsive_get_options();
if ( isset( $responsive_options['home-widgets'] ) && $responsive_options['home-widgets'] != '1' ) {
	?>
	<?php responsive_widgets_before(); // above widgets container hook ?>
<div class="content-outer">
	<aside id="secondary" class="home-widgets" role="complementary">
		<div id="home_widget_1" class="grid col" style="z-index: 5; width: 85%;">
		<?php responsive_widgets(); // above widgets hook ?>

		<?php dynamic_sidebar( 'home-widget-1' ) ; ?>
				

		<?php responsive_widgets_end(); // responsive after widgets hook ?>
		</div><!-- end of .col-300 -->

		<div id="home_widget_2" class="grid col">
		<?php responsive_widgets(); // responsive above widgets hook ?>

		<?php dynamic_sidebar( 'home-widget-2' ) ; ?>

		<?php responsive_widgets_end(); // after widgets hook ?>
		</div><!-- end of .col-300 -->

		<div id="home_widget_3" class="grid col fit">
		<?php responsive_widgets(); // above widgets hook ?>

		<?php dynamic_sidebar( 'home-widget-3' ) ; ?>

		<?php responsive_widgets_end(); // after widgets hook ?>
		</div><!-- end of .col-300 fit -->
	</aside><!-- end of #secondary -->
</div>
	<?php responsive_widgets_after(); // after widgets container hook
}
?>
