<?php

/** Sets up the WordPress Environment. */
require(dirname(__FILE__) . '/wp-load.php');

add_action('wp_head', 'wp_no_robots');

require(dirname(__FILE__) . '/wp-blog-header.php');

nocache_headers();

global $randev_state_ow;
$randev_state_ow = true;
get_header();

?>

    <script>
        jQuery(function () {
            jQuery(".content-outer.description_top").css('display', 'none');
            jQuery(".home_bloc_img_ow_content").css('display', 'none');
        });
    </script>

    <div class="row geopix_main_content"
         style="width:90%;">

        <!-- CSS -->
        <link href="<?= $base_url_randev; ?>utils/custom.css"
              rel="stylesheet">

        <!-- Font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
              rel="stylesheet">


        <div id="geopix_header_content"
             class="ml-5 w-100 mb-5"
             style="z-index: 9 !important;">

            <h1 class="mt-5">Lexique</h1>

            <div class="row alphabetlist pagination text-center ml-5"
                 data-config>
                <button class="btn"
                        value=""
                        onclick="filter_ow('all');">
                    Tout
                </button>
                <button class="btn"
                        value="A"
                        onclick="filter_ow('A');">
                    A
                </button>
                <button class="btn"
                        value="B"
                        onclick="filter_ow('B');">
                    B
                </button>
                <button class="btn"
                        value="C"
                        onclick="filter_ow('C');">
                    C
                </button>
                <button class="btn"
                        value="D"
                        onclick="filter_ow('D');">
                    D
                </button>
                <button class="btn"
                        value="E"
                        onclick="filter_ow('E');">
                    E
                </button>
                <button class="btn"
                        value="F"
                        onclick="filter_ow('F');">
                    F
                </button>
                <button class="btn"
                        value="G"
                        onclick="filter_ow('G');">
                    G
                </button>
                <button class="btn"
                        value="H"
                        onclick="filter_ow('H');">
                    H
                </button>
                <button class="btn"
                        value="I"
                        onclick="filter_ow('I');">
                    I
                </button>
                <button class="btn"
                        value="J"
                        onclick="filter_ow('J');">
                    J
                </button>
                <button class="btn"
                        value="K"
                        onclick="filter_ow('K');">
                    K
                </button>
                <button class="btn"
                        value="L"
                        onclick="filter_ow('L');">
                    L
                </button>
                <button class="btn"
                        value="M"
                        onclick="filter_ow('M');">
                    M
                </button>
                <button class="btn"
                        value="N"
                        onclick="filter_ow('N');">
                    N
                </button>
                <button class="btn"
                        value="O"
                        onclick="filter_ow('O');">
                    O
                </button>
                <button class="btn"
                        value="P"
                        onclick="filter_ow('P');">
                    P
                </button>
                <button class="btn"
                        value="Q"
                        onclick="filter_ow('Q');">
                    Q
                </button>
                <button class="btn"
                        value="R"
                        onclick="filter_ow('R');">
                    R
                </button>
                <button class="btn"
                        value="S"
                        onclick="filter_ow('S');">
                    S
                </button>
                <button class="btn"
                        value="T"
                        onclick="filter_ow('T');">
                    T
                </button>
                <button class="btn"
                        value="U"
                        onclick="filter_ow('U');">
                    U
                </button>
                <button class="btn"
                        value="V"
                        onclick="filter_ow('V');">
                    V
                </button>
                <button class="btn"
                        value="W"
                        onclick="filter_ow('W');">
                    W
                </button>
                <button class="btn"
                        value="X"
                        onclick="filter_ow('X');">
                    X
                </button>
                <button class="btn"
                        value="Y"
                        onclick="filter_ow('Y');">
                    Y
                </button>
                <button class="btn"
                        value="Z"
                        onclick="filter_ow('Z');">
                    Z
                </button>
            </div>

            <?php
            global $wpdb;
            global $wp_query;
            $gId = $_GET['galleryid']; //var_dump($gId);
            $itemId = $_GET['itemid']; //var_dump($gId);

            $queryGalleryById = " SELECT * FROM lexique ORDER BY mot ASC ";
            $galleryObj = $wpdb->get_results($wpdb->prepare($queryGalleryById));
            ?>

            <table class="table table-striped" id="tableFilterOW">
                <tbody>
                <?php foreach($galleryObj as $objItem) { ?>
                <tr class="allitem <?= strtoupper(mb_substr($objItem->mot, 0, 1, "UTF-8")) ?>">
                    <th scope="row"><?= $objItem->mot ?></th>
                    <td><?= $objItem->desc ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>


            <script>
                function filter_ow(letter){
                    //alert(letter);
                    jQuery("#tableFilterOW .allitem").css('display', 'none');
                    if (letter == "all") jQuery("#tableFilterOW .allitem").css('display', 'table-row');
                    else jQuery("#tableFilterOW .allitem."+letter).css('display', 'table-row');
                }
                jQuery(function () {
                    jQuery("#tableFilterOW .allitem").css('display', 'none');
                    jQuery("#tableFilterOW .allitem.A").css('display', 'table-row');
                    jQuery(".content-outer.description_top").css('display', 'none');
                    jQuery(".home_bloc_img_ow_content").css('display', 'none');
                });
            </script>

            <style>
                #geopix_header_content .alphabetlist .btn {
                    font-size: 2rem;
                }
            </style>

        </div>



    </div>



<?php
get_footer();
?>